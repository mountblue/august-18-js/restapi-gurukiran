const express = require('express');

const path = require('path');

const app = express();
const dbModules = require('./dbModules');

app.use(express.static(path.join(__dirname, 'public')));


app.get('/api/lists', (req, res) => { // Get details of list
  dbModules.getList().then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.get('/api/lists/:id', (req, res) => { // Get specified list
  dbModules.getSpecList(req.params.id).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.get('/api/lists/:id/items', (req, res) => { // Get all the items under specified list
  dbModules.getAllItems(req.params.id).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.post('/api/lists/:id/items', (req, res) => { // Create new item under specific list
  dbModules.createNewItem(req.query.NAME, req.params.id).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.post('/api/lists', (req, res) => { // Create new list
  dbModules.createNewList(req.query.NAME).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.delete('/api/lists/:id', (req, res) => { // Delete specified list
  dbModules.deleteList(req.params.id).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.get('/api/lists/:listId/items/:itemId', (req, res) => { // Get specified item under specified list
  dbModules.getSpecItem(req.params.listId, req.params.itemId).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.put('/api/lists/:listId/items/:itemId', (req, res) => { // Update specified item under specified list
  dbModules.updateSpecItem(req.query.CONTENT, req.params.listId, req.params.itemId, req.query.STATUS).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

app.delete('/api/lists/:listId/items/:itemId', (req, res) => { // Delete specified item under specified list
  dbModules.deleteSpecItem(req.params.listId, req.params.itemId).then((data) => {
    res.send(data);
  }).catch((err) => {
    res.status(400);
    res.send(err);
  });
});

// Assigning the  port
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening at port ${port}`));
