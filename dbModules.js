const sqlite3 = require('sqlite3').verbose();

const db = new sqlite3.Database('./lists.db', sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connected to the lists database.');
});

function getList() {
  const lists = [];
  const sql = 'SELECT * FROM LISTS';
  return new Promise((resolve, reject) => {
    db.all(sql, [], (err, rows) => {
      if (err) {
        reject(err);
      }
      rows.forEach((row) => {
        const listName = {
          ID: row.ID,
          NAME: row.NAME,
        };
        lists.push(listName);
      });
      resolve(lists);
    });
  });
}
function getSpecList(id) {
  return new Promise((resolve, reject) => {
    db.all('SELECT NAME FROM LISTS WHERE ID=(?)', [id], (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}
function getAllItems(id) {
  return new Promise((resolve, reject) => {
    db.all('SELECT * FROM LIST_ITEMS WHERE LIST_ID=(?)', [id], (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}
function createNewItem(name, id) {
  return new Promise((resolve, reject) => {
    const status = 'Incomplete';
    db.run('INSERT INTO LIST_ITEMS(CONTENT,LIST_ID,STATUS) VALUES(?,?,?)', [name, id, status], (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
    });
  });
}
function createNewList(name) {
  return new Promise((resolve, reject) => {
    db.run('INSERT INTO LISTS(NAME) VALUES(?)', [name], (err, data) => {
      if (err) {
        reject(err);
      }
      // get the last insert id
      resolve(data);
    });
  });
}
function deleteList(id) {
  return new Promise((resolve, reject) => {
    db.run('DELETE FROM LISTS WHERE ID=(?)', [id], (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
      return data;
    });
  });
}
function getSpecItem(listId, itemID) {
  return new Promise((resolve, reject) => {
    db.all('SELECT CONTENT FROM LIST_ITEMS WHERE LIST_ID=(?) AND ID=(?)', [listId, itemID], (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
      return data;
    });
  });
}
function updateSpecItem(content, listId, itemId, status) {
  return new Promise((resolve, reject) => {
    if (status === undefined) {
      db.run('UPDATE LIST_ITEMS SET CONTENT=(?) WHERE LIST_ID=(?) AND ID=(?)', [content, listId, itemId], (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
        return data;
      });
    }
    if (content === undefined) {
      db.run('UPDATE LIST_ITEMS SET STATUS=(?) WHERE LIST_ID=(?) AND ID=(?)', [status, listId, itemId], (err, data) => {
        if (err) {
          reject(err);
        }
        resolve(data);
        return data;
      });
    }
  });
}
function deleteSpecItem(listId, itemId) {
  return new Promise((resolve, reject) => {
    db.run('DELETE FROM LIST_ITEMS WHERE LIST_ID=(?) AND ID=(?)', [listId, itemId], (err, data) => {
      if (err) {
        reject(err);
      }
      resolve(data);
      return data;
    });
  });
}

module.exports = {
  getList,
  getSpecList,
  createNewItem,
  createNewList,
  deleteList,
  getSpecItem,
  updateSpecItem,
  deleteSpecItem,
  getAllItems,
};
