$(document).ready(() => {
  const trelloLists = [];
  const trelloListNames = [];
  const getLists = '/api/lists';
  fetch(getLists)
    .then(res => res.json())
    .then((data) => {
      data.forEach((listIDs) => {
        trelloLists.push(listIDs.ID);
        trelloListNames.push(listIDs.NAME);
      });
      for (let i = 0; i < trelloLists.length; i += 1) {
        $('#list-select').append(`<option  value="${trelloLists[i]}">${trelloListNames[i]}</option>`);
        const getItem = `/api/lists/${trelloLists[i]}/items`;
        fetch(getItem)
          .then(res => res.json())
          .then((dataItems) => {
            dataItems.forEach((Items) => {
              if (Items.STATUS === 'Incomplete') {
                $('#form-undone-tasks').append(`<label><input value="${Items.ID}|${Items.LIST_ID}" class="cb pristine" type="checkbox"> <span>${Items.CONTENT}</span></label>`);
              }
              if (Items.STATUS === 'Complete') {
                $('#form-undone-tasks').append(`<label><input value="${Items.ID}|${Items.LIST_ID}" class="cb pristine" type="checkbox" checked> <span><strike>${Items.CONTENT}</strike></span></label>`);
              }
              const q = document.querySelectorAll('.cb');
              for (const j in q) {
                if (+j < q.length) {
                  q[j].addEventListener('click', function checkboxCode() {
                    const c = this.classList;
                    const p = 'pristine';
                    if (c.contains(p)) {
                      c.remove(p);
                    }
                  });
                }
              }
            });
            const checkboxes = $('input[type="checkbox"]');
            checkboxes.change(function taskStatus() {
              const values = this.value.split('|');
              const listID = values[1];
              const itemID = values[0];
              const state = this.checked ? 'Complete' : 'Incomplete';
              const putURL = `/api/lists/${listID}/items/${itemID}?STATUS=${state}`;
              fetch(putURL, {
                method: 'PUT',
              });
            });
          });
      }
    });
});


function submitTask() {
  const taskName = document.getElementById('typeTask').value;
  const listID = document.getElementById('list-select').value;
  const postURL = `http://127.0.0.1:3000/api/lists/${listID}/items?NAME=${taskName}`;
  fetch(postURL, {
    method: 'POST',
  });
}
